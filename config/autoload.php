<?php

/**
 * Contao Open Source CMS
 *
 * Copyright (c) 2005-2014 Leo Feyer
 *
 * @package CampainLayer
 * @link    https://contao.org
 * @license http://www.gnu.org/licenses/lgpl-3.0.html LGPL
 */


/**
 * Register the classes
 */
ClassLoader::addClasses(array
(
	'ModuleCampainLayer' => 'system/modules/campainLayer/ModuleCampainLayer.php',
));


/**
 * Register the templates
 */
TemplateLoader::addFiles(array
(
	'cl_layer' => 'system/modules/campainLayer/templates',
));
